%include "lib.inc"

section .text

global find_word

; Принимает указатель на нуль-терминированную строку и указатель на начало словаря
; rdi - указатель на строку, rsi - указатель на начало словаря
; Возвращает адрес начала вхождения в словарь, иначе 0
; rax - адрес начала вхождения в словарь
find_word:
    xor rax, rax
    .loop:
        push    rsi 
        push    rdi
        add     rsi, 8     ; прибавляем 8, так как первые 8 байт занимает указатель на следующий элемент
        
        call    string_equals

        pop     rdi
        pop     rsi
        test    rax, rax
        jnz     .success
        cmp     qword[rsi], 0   ; является ли указатель на следующий элемент нулевым указателем
        je      .fail
        
        mov     rsi, [rsi]          ; переход к следующему элементу
        jmp     .loop

    .success:
        mov     rax, rsi
        add     rax, 8
    .fail:     
        ret
