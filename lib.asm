%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define SYSTEMCALL_WRITE 1
%define SYSTEMCALL_READ 0
%define SYSTEMCALL_EXIT 60

section .text

global exit
global string_length
global print_error
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSTEMCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    jz .break
    inc rax 
    jmp .loop
.break:
    ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
	push	rdi
	call	string_length
	pop	rsi
	mov	rdx, rax
	mov	rax, SYSTEMCALL_WRITE
	mov	rdi, STDERR
	syscall
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSTEMCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYSTEMCALL_WRITE
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    dec rsp
    mov byte[rsp], 0
    mov r9, 10
.loop:
    xor rdx, rdx
    div r9
    add rdx, '0'
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    ja .loop
    mov rdi, rsp
    push r8
    call print_string
    pop r8
    mov rsp, r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.positive:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r9, r9
.loop:
    mov r8b, byte[rdi+r9]
    cmp r8b, byte[rsi+r9]    
    jnz .break
    inc r9
    test r8b, r8b
    jnz .loop
    mov rax, 1
.break:
    ret
	

read_char:
    push 0
    mov rax, SYSTEMCALL_READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    jz .ret
    pop rax
    ret
.ret:
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    test rsi, rsi
    jz .error
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
.whitespace_characters:
    call .checker_if_space
    test rax, rax
    jz .end
    cmp al, 1
    jz .whitespace_characters
    xor r14, r14
.loop:
    test rax, rax
    jz .end
    cmp al, 1
    jz .end
    cmp r13, r14
    jz .error
    mov byte[r12+r14], al
    inc r14
    call .checker_if_space
    jmp .loop
.end:
    mov byte[r12+r14], 0
    mov rax, r12
    mov rdx, r14
.ret:
    pop r14
    pop r13
    pop r12
    ret
.error:
    xor rax, rax
    jmp .ret
.checker_if_space:
    call read_char
    cmp al, ` `
    jz .return_true
    cmp al, `\t`
    jz .return_true
    cmp al, `\n`
    jz .return_true
.return_false:
    ret
.return_true:
    mov rax, 1
    ret
	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_line:
	push	rbx
	push	r12
	push	r13
	mov	rbx, -1		; Место для нуль-терминатора
	mov	r12, rsi
	mov	r13, rdi
	.read_spaces:
		call	read_char
		test	al, al
		jz	.end_bad
		cmp 	al, ` `
		je	.read_spaces
		cmp	al, `\n`
		je	.read_spaces
		cmp 	al, `\t`
		je	.read_spaces
		jmp	.skip_test
	.read_word:
		cmp	al, `\n`
		je	.end_good
		cmp	al, `\t`
		je	.end_good
		test	al, al
		je	.end_good
	.skip_test:    ; Чтобы избежать повторной проверки
		inc 	rbx
		cmp	rbx, r12
		je	.end_bad
		mov	byte [r13 + rbx], al
		call	read_char
		jmp	.read_word
	.end_good:
		inc	rbx
		mov	byte [r13 + rbx], 0
		mov	rax, r13
		mov	rdx, rbx
		pop	r13
		pop	r12
		pop	rbx
		ret
	.end_bad:
		xor 	rax, rax
		xor	rdx, rdx
		pop	r13
		pop 	r12
		pop	rbx
		ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov r8b, byte[rdi]
    test r8b, r8b
    jz .break
    cmp r8, '9'
    ja .break
    cmp r8, '0'
    jb .break
    sub r8, '0'
    imul rax, rax, 10
    jc .error
    add rax, r8
    jc .error
    inc rdi
    inc rdx
    jmp .loop
.error:
    xor rdx, rdx
.break:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    jz .minus
    cmp byte[rdi], '+'
    jnz .nosign
.plus:
    inc rdi
    call parse_uint
    inc rdx
    ret
.nosign:    
    jmp parse_uint
.minus:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    jge .error
    mov r8b, byte[rdi+rax]
    mov byte[rsi+rax], r8b
    test r8b, r8b
    inc rax
    jnz .loop
    ret
.error:
    xor rax, rax
    ret